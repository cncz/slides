layout: true
class: center, middle
---
# Faculty of Science IT services

## Introduction for new staff members

Bram Daams, February 20, 2024

???
<!-- update this slideshow on GitLab https://gitlab.science.ru.nl/cncz/slides -->

# Welcome
## My name is ...

I'd like to tell you the basic information that you need to utilize the IT services that are
available to you as a staff member of the Faculty of Science.

---

# C&CZ
## Computer & Communicatiezaken

???
Google cncz
---
# For whom?

???
- For students and staff of the Faculty of Science
---
# HG00.051
## Huygens building, ground floor, start of North street, wing 5

.center[<img src="img/floorplan.jpg" width="350">]
???
- We are located in the Huygens building

---
# 024 - 36 20000
## a human being for all questions
---

# [helpdesk@science.ru.nl](helpdesk@science.ru.nl)
## contact by mail

---

# Science login
## no dots, no `@`, no caps

???
- Students & Staff
- requested by another staff member
- no relation with your RU account
- required for faculty specific services
- when created, a welcome mail is sent with some information

---
# Science services
## google `cncz` & one of these topics
.pull-left[
.left[
### .icofont-shield-alt[] [diy.science.ru.nl](https://dhz.science.ru.nl)
### .icofont-ui-email[]   mail
### .icofont-database[]   storage
### .icofont-code[]       GitLab
### .icofont-server[]     compute cluster
]]

.pull-right[
.left[
### .icofont-computer[]        computer labs
### .icofont-network[]         vpn
### .icofont-console[]         ssh
### .icofont-question-circle[] surveys
### .icofont-web[]             hosting
]]


???
- Do it yourself: account & authorization & mail management
- Science mail, webmail
- Backed up network storage: homedir + shares for departments/groups
- GitLab: gitlab.science.ru.nl and mattermost (chat / slack alternative)
- High performance computing cluster

- Computer labs and BYOD
- choice of VPNs: - VPNsec, in most cases just works (no config needed) or OpenVPN
- Linux Login server: lilo.science.ru.nl (ssh)
- Survey server for questionaires for research or for evaluation of education

---
class: middle
# RU account
## In transition... 

--

.pull-left[
.left[
## from `U123456`
### .icofont-id-card[]       portal 
### .icofont-shopping-cart[] BASS 
### .icofont-shield-alt[]    account 
### .icofont-black-board[]   brightspace
]]

.pull-right[
.left[
## to `Your.Name@ru.nl`
### .icofont-ui-email[] mail
### .icofont-brand-windows[] ms365
]]
???
- RU Mail and Calendar: https://mail.ru.nl , MS Exchange/Outlook
- all RU services will follow in 2022
- RU Portal: https://portal.ru.nl with salary, day code for wifi for guests, ...
- BASS: https://bass.ru.nl for administering vacation days, flex work conditions, purchases, reimbursements, ... 
- Account: https://account.ru.nl for RU password change, Brightspace authorization, ...
- Brightspace: https://brightspace.ru.nl DLE for courses

---
# Intake

