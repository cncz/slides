<!-- update this slideshow on GitLab https://gitlab.science.ru.nl/cncz/slides -->

# Faculty of Science IT services

Introduction for new staff members

Peter van Campen, November 19, 2020

---

# C&CZ
`"Computer & Communicatiezaken"`

Faculty of Science IT expertise and support

- For all scientific staff and students
- Location: HG00.545 (on the ground floor, start of North street, wing 5)

> Helpdesk:   (+31 24 36) 20000, helpdesk@science.ru.nl
>
> Sysadmins:  (+31 24 36) 53535, postmaster@science.ru.nl
>

> google for cncz+searchitem, e.g. login, mail, software, do it yourself, printing, wireless, ...


---

# Science login

- All Faculty of Science staff (and students) have a science login
	- Usually consists of first name or initial(s) and last name
	- Does not contain a dot, capitals or @
	- Is separate from RU account (u-number)
- Gives access to Faculty of Science IT services
	- Mail (science.ru.nl)
	- Backed up storage: homedir + shares for departments/groups
	- Computer labs (future: BYOD) and compute cluster nodes
	- Linux Login server: lilo.science.ru.nl (ssh)
	- choice of VPNs:
		- VPNsec, in most cases just works (no config needed)
		- OpenVPN alternative if problems with VPNsec
	- GitLab: gitlab.science.ru.nl
	- Survey server


---

# Science Mail

- Science Mail with addresses of the form: I.Surname@science.ru.nl
- Can be forwarded to e.g. Givenname.Surname@ru.nl on the DIY website
- Extra mail addresses / mailing lists can be requested. Manage these on DIY

- See [webmail.science.ru.nl](https://webmail.science.ru.nl) for details

> Google cncz+mail

---

# RU services accessed with RU mail address

- RU Mail and Calendar: https://mail.ru.nl , MS Exchange/Outlook
- all RU services will follow in 2021

---

# RU services accessed with U-number

- RU Portal: https://portal.ru.nl with salary, day code for wifi for guests, ...
- BASS: https://bass.ru.nl for administering vacation days, flex work conditions, purchases, reimbursements, ... 
- Account: https://account.ru.nl for RU password change, Brightspace authorization, ...
- Brightspace: https://brightspace.ru.nl DLE for courses

---
# Security

- Make sure important data is backed up
- Never tell or mail your username/password to anyone
- Ignore phishing mails which typically invite/urge/threaten you to
    - Mail your logon credentials to "the system administrators"
    - Logon to a site disguised as something familiar, e.g. to "validate your credentials" or "upgrade your account"
- Don't click on / open attachments in mails from just anyone
- Be very careful on unknown websites; any click may infect your computer
- Never forget to log off

---
# Data breaches

- Report possible security problems to C&CZ
- Data breaches must be reported!
- Don't be the source of a data breach yourself. E.g.: encrypt your laptop!
- EU law about privacy and personal data: GDPR/AVG (General Data Protection Regulation)

---
# Questions?

Time left for questions?

---

# Wifi and Mobile phones

- Connect to the "eduroam" wireless networks at educational institutes worldwide
    - Log in with *Unumber*@ru.nl   

- Preferred mobile operator for Huygens (Faraday cage): Vodafone

> Google cncz+wifi

---

# Software

- Campus licenses for many software packages
    - Rights differ per package, ask postmaster for details
- Most packages are available on the "Install share"
    - `\\install-srv.science.ru.nl\install`
    - Ask C&CZ helpdesk for the license key
- Many software packages for personal use via [surfspot.nl](http://surfspot.nl)
    - Friendly pricing for RU employees and students

> Google cncz+software

---

# Computer Labs and C&CZ-managed workstations

- All computer lab PCs are dual-boot: MS Windows 10 / Ubuntu Linux 18.04
- Laptops with MS Windows 10 can be borrowed at the Library of Science
- C&CZ can also manage your Windows or Ubuntu Linux workstation. With Windows you can also get a local admin account
- Contact us for software requests
- Home-directory available everywhere
    - In Windows this is the U: disk
    - Default quota is 5 Gbyte, can be increased if necessary 

> Google cncz+"computer lab"

---

# DIY - Do It Yourself

Manage your science login settings on :

> http://diy.science.ru.nl/

For example:

- Science password
- Groups to share directories on network drives that are backed up Daily/Monthly/Yearly
- Forwarding of mail
- Mailing lists
- Out of office reply
- Spam settings, whitelist

---
# Printing

- Péage: RU print/scan/copy system
- Costs paid by your department
- Follow me printing
- Log in with a coupled chip card (e.g. campus card)
- Scan to me

> Google cncz+peage

> www.ru.nl/peage

- Poster or canvas and 3D printing service at the C&CZ office

> Google cncz+poster or cncz+3D
---

# (More) things to Google

> cncz+mail 
>
> cncz+"computer lab"
>
> cncz+software
>
> cncz+peage
>
> cncz+wireless
>
> cncz+vpn
>
> cncz+"disk space"

