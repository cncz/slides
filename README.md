# Introductie nieuwe medewerker/student

Deze slides worden gebruikt bij de introductie van nieuwe medewerkers, studenten
en vwo-talenten. C&CZ verzorgt hier een praatje van ongeveer een kwartier.

## Linkjes

| bestand                       | url                                                          |
| --                            | --                                                           |
| staff-introduction-full.md    | <https://cncz.pages.science.ru.nl/slides/staff.html>         |
| staff-introduction-compact.md | <https://cncz.pages.science.ru.nl/slides/staff-compact.html> |
| vwo-introduction-full.md      | <https://cncz.pages.science.ru.nl/slides/vwo.html>           |
| vwo-introduction-compact.md   | <https://cncz.pages.science.ru.nl/slides/vwo-compact.html>   |


---

# Bijwerken

Op https://gitlab.science.ru.nl/cncz/slides/ bewerken of deze repository clonen en lokaal bewerken.
In de `.gitlab-ci.yml` staat beschreven op welke manier de `.md` files worden samengesteld
tot `.html`-bestanden.

## Verschillende doelgroepen synchroon houden
Dit kan vrij overzichtelijk in `meld` waarbij je de verschillen ziet en ook in beide bestanden kunt editen.

Bijvoorbeeld:

```
meld staff-introduction-compact.md vwo-introduction-compact.md
```
