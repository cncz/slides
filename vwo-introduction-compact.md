layout: true
class: center, middle
---
# Faculty of Science ICT-diensten

## Introductie voor scholieren (NLT/VWO)

???
<!-- update this slideshow on GitLab https://gitlab.science.ru.nl/cncz/slides -->

# Welcome
## My name is ...

I'd like to tell you the basic information that you need to utilize the IT services that are
available to you as a staff member of the Faculty of Science.

---

# C&CZ
## Computer & Communicatiezaken

???
Google cncz
---
# Voor wie?

???
- For students and staff of the Faculty of Science
---
# HG00.051
## Huygensgebouw
## Begane grond
## begin van de Noordstraat
## Vleugel 5


???
- We are located in the Huygens building

---
# 024 - 36 20000
## een luisterend oor voor alle vragen
---

# [helpdesk@science.ru.nl](helpdesk@science.ru.nl)
## contact via mail

---

# Science login
## geen punten, geen `@`, geen hoofdletters

???
- Students & Staff
- requested by another staff member
- no relation with your RU account
- required for faculty specific services
- when created, a welcome mail is sent with some information

---
# Science diensten
## google `cncz` & een onderwerp:
.pull-left[
.left[
### .icofont-shield-alt[] [dhz.science.ru.nl](https://dhz.science.ru.nl)
### .icofont-ui-email[]   mail
### .icofont-database[]   storage
### .icofont-code[]       GitLab
### .icofont-server[]     rekencluster
]]

.pull-right[
.left[
### .icofont-computer[]        pc cursuszalen
### .icofont-network[]         vpn
### .icofont-console[]         ssh
### .icofont-question-circle[] enquetes
### .icofont-web[]             hosting
]]


???
- Do it yourself: account & authorization & mail management
- Science mail, webmail
- Backed up network storage: homedir + shares for departments/groups
- GitLab: gitlab.science.ru.nl and mattermost (chat / slack alternative)
- High performance computing cluster

- Computer labs (future: BYOD)
- choice of VPNs: - VPNsec, in most cases just works (no config needed) or OpenVPN
- Linux Login server: lilo.science.ru.nl (ssh)
- Survey server for questionaires for research or for evaluation of education

---
class: middle
# RU account
## verandert... 

--

.pull-left[
.left[
## van `s12345678` (VWO) of `E123456` (NLT)
### .icofont-id-card[]       portal  
### .icofont-shield-alt[]    account 
### .icofont-black-board[]   brightspace
]]

.pull-right[
.left[
## naar `Your.Name@ru.nl`
### .icofont-ui-email[] mail
]]
???
- RU Mail and Calendar: https://mail.ru.nl , MS Exchange/Outlook
- all RU services will follow
- RU Portal: https://portal.ru.nl with salary, day code for wifi for guests, ...
- BASS: https://bass.ru.nl for administering vacation days, flex work conditions, purchases, reimbursements, ... 
- Account: https://account.ru.nl for RU password change, Brightspace authorization, ...
- Brightspace: https://brightspace.ru.nl DLE for courses

---

# Wifi en mobiele telefoons
## verbind met `Eduroam`
## kooi van Faraday, maar VoWiFi (wifi bellen)

???

- Connect to the "eduroam" wireless networks at educational institutes worldwide
    - Log in with *Snumber*@ru.nl or *Eumber*@ru.nl*
          
- Preferred mobile operator for Huygens (Faraday cage): Vodafone

---

# Beveiliging
## bescherm je gegevens
```
0000000 2a2f 200a 4220 696c 6b6e 0a0a 2020 7554
0000010 6e72 2073 6e61 4c20 4445 6f20 206e 6f66
0000020 2072 6e6f 2065 6573 6f63 646e 202c 6874
0000030 6e65 6f20 6666 6620 726f 6f20 656e 7320
0000040 6365 6e6f 2c64 7220 7065 6165 6574 6c64
0000050 2e79 0a0a 2020 6f4d 7473 4120 6472 6975
0000060 6f6e 2073 6168 6576 6120 206e 6e6f 622d
0000070 616f 6472 4c20 4445 7920 756f 6320 6e61
0000080 6320 6e6f 7274 6c6f 202e 6e4f 7420 6568
0000090 5520 4f4e 202c 454d 4147 6120 646e 5a20
00000a0 5245 0a4f 2020 7469 6920 2073 7461 6174
00000b0 6863 6465 7420 206f 6964 6967 6174 206c
00000c0 6970 206e 3331 202c 6e6f 4d20 524b 3031
00000d0 3030 6f20 206e 6970 206e 2e36 4c20 4445
00000e0 425f 4955 544c 4e49 6920 2073 6573 2074
00000f0 6f74 200a 7420 6568 6320 726f 6572 7463
0000100 4c20 4445 7020 6e69 6920 646e 7065 6e65
0000110 6564 746e 6f20 2066 6877 6369 2068 6f62
0000120 7261 2064 7369 7520 6573 2e64 200a 4920
0000130 2066 6f79 2075 6177 746e 7420 206f 6e6b
0000140 776f 7720 6168 2074 6970 206e 6874 2065
0000150 6e6f 622d 616f 6472 4c20 4445 6920 2073
```
???
- Make sure important data is backed up
- Never tell or mail your username/password to anyone
- Ignore phishing mails which typically invite/urge/threaten you to
    - Mail your logon credentials to "the system administrators"
    - Logon to a site disguised as something familiar, e.g. to "validate your cr
- Don't click on / open attachments in mails from just anyone
- Be very careful on unknown websites; any click may infect your computer
- Never forget to log off
---

# Datalek
## moet gemeld aan C&CZ

???
- Report possible security problems to C&CZ
- Data breaches must be reported!
- Don't be the source of a data breach yourself. E.g.: encrypt your laptop!
- EU law about privacy and personal data: GDPR/AVG (General Data Protection Regu

---
# Vragen?
## elk onderwerp

---

# Software (bonus)
## erg divers & [surfspot.nl](https://surfspot.nl)

???
- Campus licenses for many software packages
- Rights differ per package, ask postmaster for details
- Most packages are available on the "Install share"
    - `\\install-srv.science.ru.nl\install`
    - Ask C&CZ helpdesk for the license key
- Many software packages for personal use via [surfspot.nl](http://surfspot.nl), e.g.
    - upgrade to Windows 10 Education with Bitlocker encryption for your laptop
    - F-Secure security software
 Very friendly pricing for RU employees and students

> Google cncz+software

---

# PC-onderwijsruimtes (bonus)
## dual-boot: MS Windows 10 / Ubuntu Linux 22.04
## BYOD en leenlaptops
???

- BYOD is the future: all computer labs will be phased out
- All computer lab PCs are dual-boot: MS Windows 10 / Ubuntu Linux 20.04
- Laptops with MS Windows 10 can be borrowed at the Library of Science
- C&CZ can also manage your Windows or Ubuntu Linux workstation. With Windows you can also get a local admin account
- Contact us for software requests
- Home-directory available everywhere
    - In Windows this is the U: disk
    - Default quota is 5 Gbyte, can be increased if necessary 

> Google cncz+"computer lab"

---

# DIY  (bonus)
## snelle demo

???
Manage your science login settings on :

> http://diy.science.ru.nl/

For example:

- Science password
- Groups to share directories on network drives that are backed up Daily/Monthly/Yearly
- Forwarding of mail
- Mailing lists
- Out of office reply
- Spam settings, whitelist

---
# Printen: Péage (bonus)
## print naar een printer
## haal het op bij een willekeurige multifunctional

???

- Péage: RU print/scan/copy system
- Costs paid by your department
- Follow me printing
- Log in with a coupled chip card (e.g. campus card)
- Scan to me

> Google cncz+peage

> www.ru.nl/peage

- Poster or canvas and 3D printing service at the C&CZ office

> Google cncz+poster or cncz+3D
---
layout: false
class: middle, left

# Googlen (bonus)
.pull-left[
## `cncz+mail`  
## `cncz+"computer lab"`
## `cncz+software`
## `cncz+peage`
]
.pull-right[
## `cncz+wireless`
## `cncz+vpn`
## `cncz+storage`
## `cncz+login`
]
